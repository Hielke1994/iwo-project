#!/bin/sh
#This script will get tweets, and search for certain words and determain the gender of the tweeter and give out with how many man and woman used the word on twitter.

#NOTE: I used a own directory on my pc with some downloaded tweets since I could not connect my pc to the network via Bastion. Therefore the exact data is not replicable with this script.

for file in twitter2/Tweets/2017/03/*; do zless "$file" | twitter2/tools/tweet2tab text user >> output.txt
done

totalGender=`cat output.txt | grep -P -o "\t.+"| ./get-gender.py > gender.txt`
Man=`cat gender.txt | grep -E -w -c "male"`
Vrouw=`cat gender.txt | grep -E -w -c "female"`
echo totaal mannentweets $Man
echo totaal vrouwentweets $Vrouw


Biertweet=`cat output.txt | grep "bier" > out.txt`
Gender=`cat out.txt  | grep -P -o "\t.+"| tr -d '\t' | ./get-gender.py > gender.txt`
Manbier=`cat gender.txt | grep -E -w -c "male"`
Vrouwbier=`cat gender.txt | grep -E -w -c "female"`
echo mannen met bier $Manbier
echo vrouwen met bier $Vrouwbier


Wijntweet=`cat output.txt | grep "wijn" > out.txt`
Gender=`cat out.txt  | grep -P -o "\t.+"| tr -d '\t' | ./get-gender.py > gender.txt`
Manwijn=`cat gender.txt | grep -E -w -c "male"`
Vrouwwijn=`cat gender.txt | grep -E -w -c "female"`
echo mannen met wijn $Manwijn
echo vrouwen met wijn $Vrouwwijn


Voetbaltweet=`cat output.txt | grep "voetbal" > out.txt`
Gender=`cat out.txt  | grep -P -o "\t.+"| tr -d '\t' | ./get-gender.py > gender.txt`
Manvoetbal=`cat gender.txt | grep -E -w -c "male"`
Vrouwvoetbal=`cat gender.txt | grep -E -w -c "female"`
echo mannen met voetbal $Manvoetbal
echo vrouwen met voetbal $Vrouwvoetbal


Yogatweet=`cat output.txt | grep "yoga" > out.txt`
Gender=`cat out.txt  | grep -P -o "\t.+"| tr -d '\t' | ./get-gender.py > gender.txt`
Manyoga=`cat gender.txt | grep -E -w -c "male"`
Vrouwyoga=`cat gender.txt | grep -E -w -c "female"`
echo mannen met yoga $Manyoga
echo vrouwen met yoga $Vrouwyoga

>output.txt
>out.txt
