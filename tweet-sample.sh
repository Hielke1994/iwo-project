#!/bin/bash
#This script will find tweets, check for how mant unique tweets there are, how many retweets there are and #prints the first 20 unique tweets that are not retweets.

#Counts the tweets
# find tweets | counts lines
Tweet=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | wc -l`
echo There are $Tweet tweets.

#filters duplicate tweets
# Finds tweets | Finds text element in tweet | sort the lines | filter duplicates | count lines
UniqTweet=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text |sort | uniq -uc| wc -l`
echo There are $UniqTweet unique tweets.

#Finds the retweets
# Find tweets | find text element | sort tweets | find tweets with RT | count lines
RTtweet=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort -u | grep ^RT | wc -l`
echo There are $RTtweet retweets.

#prints the first 20 unique tweets.
# Find tweets | Find text element | sort lines | remove duplicates | show first 20 results
FinalTweets=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text|sort|uniq -u| head -20`
echo The first 20 unique tweets are \n $FinalTweets.

